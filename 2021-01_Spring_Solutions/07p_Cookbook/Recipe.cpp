#include "Recipe.hpp"

#include <iostream>
using namespace std;

/**
Recipe constructor
Initialize m_totalIngredients to 0.
*/
Recipe::Recipe()
    : MAX_INGREDIENTS( 20 )
{
    m_totalIngredients = 0;
}

/**
Recipe m_name setter
Set the private member variable m_name to the value passed in as name.
*/
void Recipe::SetName( string name )
{
    m_name = name;
}

/**
Recipe m_name getter
Return the value from the private member variable m_name.
*/
string Recipe::GetName()
{
    return m_name;
}

/**
Recipe m_instructions setter
Set the private member variable m_instructions to the value passed in as instructions.
*/
void Recipe::SetInstructions( string instructions )
{
    m_instructions = instructions;
}

/**
Recipe m_instructions getter
Return the value from the private member variable m_instructions.
*/
string Recipe::GetInstructions()
{
    return m_instructions;
}

/**
Recipe m_source setter
Set the private member variable m_source to the value passed in as instructions.
*/
void Recipe::SetSource( string source )
{
    m_source = source;
}

/**
Recipe m_source getter
Return the value from the private member variable m_source.
*/
string Recipe::GetSource()
{
    return m_source;
}


/**
Add a new ingredient to m_ingredients using the name, amount, and unit passed in.

ERROR CHECK:    If the m_totalIngredients count is equal to 10, then the ingredient
                list is full - disallow more ingredients.
*/
void Recipe::AddIngredient( string name, float amount, string unit )
{
    if ( m_totalIngredients == MAX_INGREDIENTS )
    {
        cout << "Ingredient list is full!" << endl;
        return;
    }

    m_ingredients[m_totalIngredients].Setup( name, amount, unit );
    m_totalIngredients++;
}

/**
Display the m_name of the recipe and the m_source.
Then, iterate through all the ingredients in m_ingredients and display each one.
Afterwards, display the instructions m_instructions.
*/
void Recipe::Display()
{
    cout << "---------------------- " << m_name << " ----------------------" << endl;
    cout << "From " << m_source << endl << endl;
    cout << "----------- INGREDIENTS -----------" << endl;
    for ( int i = 0; i < m_totalIngredients; i++ )
    {
        m_ingredients[i].Display();
    }
    cout << endl;
    cout << "----------- INSTRUCTIONS -----------" << endl;
    cout << m_instructions << endl;
    cout << "--------------------------------------------------------------" << endl;
}
