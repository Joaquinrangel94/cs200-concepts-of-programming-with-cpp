#include <iostream>
using namespace std;

#include "Cookbook.hpp"

int main()
{
    // Set up our recipe book
    Cookbook cookbook;
    cookbook.LoadRecipes( "../recipes.txt" );
    cookbook.SetTitle( "STUDENTNAME's Cookbook" );
//    cookbook.DisplayRecipeList();
//    cookbook.DisplayRecipe( 0 );

    bool done = false;
    while ( !done )
    {
        cout << endl << cookbook.GetTitle() << endl;
        cout << "------------------------------------" << endl;
        cout << "Which recipe would you like to view?" << endl;
        cookbook.DisplayRecipeList();
        cout << "Or -1 to quit." << endl;

        cout << endl << ">> ";

        int choice;
        cin >> choice;

        if ( choice == -1 )
        {
            done = true;
        }
        else
        {
            cookbook.DisplayRecipe( choice );
        }
    }

    return 0;
}
