#include "Cookbook.hpp"

#include <string>
#include <fstream>
#include <iostream>
using namespace std;

/**
Cookbook constructor
Initialize m_totalRecipes to 0.
*/
Cookbook::Cookbook()
{
    m_totalRecipes = 0;
}

/**
Cookbook m_title setter
Set the private member variable m_title to the value passed in as title.
*/
void Cookbook::SetTitle( string title )
{
    m_title = title;
}

/**
Cookbook m_title getter
Return the value from the private member variable m_title.
*/
string Cookbook::GetTitle()
{
    return m_title;
}

/**
Display the list of recipe names in the m_recipes array.
*/
void Cookbook::DisplayRecipeList()
{
    for ( int i = 0; i < m_totalRecipes; i++ )
    {
        cout << i << ". " << m_recipes[i].GetName() << endl;
    }
}

/**
Display a single recipe.
*/
void Cookbook::DisplayRecipe( int index )
{
    if ( index < 0 || index >= m_totalRecipes )
    {
        cout << "ERROR: Recipe index is invalid!" << endl;
        return;
    }

    m_recipes[index].Display();
}

/**
Load a list of recipes from the file
*/
void Cookbook::LoadRecipes( string filename )
{
    string buffer;

    string recipeTitle;
    string recipeSource;
    string recipeInstructions;
    float ingredientAmount;
    string ingredientUnit;
    string ingredientName;

    ifstream input( filename );

    if ( input.fail() )
    {
        cout << "ERROR: Could not open file \"" << filename << "\"!" << endl;
        return;
    }

    while ( getline( input, buffer ) )
    {
        if      ( buffer == "TITLE" )               { getline( input, recipeTitle ); }
        else if ( buffer == "SOURCE" )              { getline( input, recipeSource ); }
        else if ( buffer == "INSTRUCTIONS" )        { getline( input, recipeInstructions ); }
        else if ( buffer == "INGREDIENT_AMOUNT" )   { input >> ingredientAmount; input.ignore(); }
        else if ( buffer == "INGREDIENT_UNIT" )     { getline( input, ingredientUnit ); }
        else if ( buffer == "INGREDIENT_NAME" )     { getline( input, ingredientName ); }

        else if ( buffer == "INGREDIENT_END" )
        {
            m_recipes[m_totalRecipes].AddIngredient( ingredientName, ingredientAmount, ingredientUnit );
        }
        else if ( buffer == "RECIPE_END" )
        {
            m_recipes[m_totalRecipes].SetName( recipeTitle );
            m_recipes[m_totalRecipes].SetInstructions( recipeInstructions );
            m_recipes[m_totalRecipes].SetSource( recipeSource );
            m_totalRecipes++;
        }
    }
}








