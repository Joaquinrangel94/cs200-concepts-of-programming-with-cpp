#include "Blogr.hpp"

#include <iostream>
using namespace std;

BlogrApp::BlogrApp()
    : MAX_ACCOUNTS( 10 ), MAX_POSTS( 10 )
{
    // Create some accounts
    m_accounts[0].SetUsername( "xXGandalfXx" );
    m_accounts[0].SetEmail( "gandalf@me.com" );
    m_accounts[0].SetPassword( "hobbits" );

    m_accounts[1].SetUsername( "CatGirl123" );
    m_accounts[1].SetEmail( "cats@cats.com" );
    m_accounts[1].SetPassword( "cats" );

    m_totalAccounts = 2;

    m_posts[0].SetText( "I like cheese." );
    m_posts[0].SetAuthorId( 0 );

    m_posts[1].SetText( "Cats R gr34t." );
    m_posts[1].SetAuthorId( 1 );

    m_totalPosts = 2;
}

void BlogrApp::Run()
{
    LoginMenu();

    bool done = false;
    while ( !done )
    {
        cout << "Logged in as " <<
            m_accounts[ m_currentAccount ].GetUsername()
            << endl << endl;
        MainMenu();
        cout << "Choice: ";
        int choice;
        cin >> choice;

        if      ( choice == 1 ) // View posts
        {
            ViewPosts();
        }
        else if ( choice == 2 ) // New post
        {
            WritePost();
        }
        else if ( choice == 3 ) // Logout
        {
            done = true;
        }
    }
}

void BlogrApp::LoginMenu()
{
    cout << "LOGIN" << endl;
    // Display accounts that they can log in as.
    DisplayAccounts();

    int choice;
    cout << "Which account? ";
    cin >> choice;

    if ( choice < 0 || choice >= m_totalAccounts )
    {
        cout << "Invalid selection, try again: ";
        cin >> choice;
    }

    string password;
    cout << "Enter password: ";
    cin.ignore();
    getline( cin, password );

    while ( password != m_accounts[ choice ].GetPassword() )
    {
        cout << "Incorrect password! Try again: ";
        getline( cin, password );
    }

    m_currentAccount = choice;
}

void BlogrApp::DisplayAccounts()
{
    // Iterate through all the accounts
    for ( int i = 0; i < m_totalAccounts; i++ )
    {
        cout << i << ". " << m_accounts[i].GetUsername() << endl;
    }
}

void BlogrApp::MainMenu()
{
    cout << endl << "MAIN MENU" << endl;
    cout << "1. View posts" << endl;
    cout << "2. Write new post" << endl;
    cout << "3. Logout" << endl;
}

void BlogrApp::ViewPosts()
{
    cout << endl << "VIEW POSTS" << endl << endl;

    // Iterate through all the accounts
    for ( int i = 0; i < m_totalPosts; i++ )
    {
        cout << "POST " << i << ":" << endl;
        cout << "\"" << m_posts[i].GetText() << "\"" << endl;
        cout << "By "
            << m_accounts[ m_posts[i].GetAuthorId() ].GetUsername()
            << endl << endl;
    }
}

void BlogrApp::WritePost()
{
    // Error check: Posts are full?
    if ( m_totalPosts == MAX_POSTS )
    {
        cout << "OUT OF SPACE" << endl;
        return;
    }

    cout << "Enter post text:" << endl;
    string temp;
    cin.ignore();
    getline( cin, temp );

    m_posts[ m_totalPosts ].SetText( temp );
    m_posts[ m_totalPosts ].SetAuthorId( m_currentAccount );
    m_totalPosts++;
}




