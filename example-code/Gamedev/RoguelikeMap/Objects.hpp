#ifndef _OBJECTS_HPP
#define _OBJECTS_HPP

struct Entity
{
    int x;
    int y;
    char symbol;

    void Setup( int x, int y, char symbol )
    {
        this->x = x;
        this->y = y;
        this->symbol = symbol;
    }
};

struct Map
{
    const int WIDTH;
    const int HEIGHT;

    char floor;
    char wall;

    char tiles[80][20];

    Map() : WIDTH(80), HEIGHT(20)
    {
        wall = '#';
        floor = ' ';
    }

    bool IsFloor( int x, int y )
    {
        return ( tiles[x][y] == floor );
    }
};

#endif
