#include <iostream>
#include <string>
#include <array>
using namespace std;

int main()
{
    string movie = "My Neighbor Totoro";

    // (Traditional array) Declare an array
    const int MAX_MOVIES = 10;
    string movies[MAX_MOVIES];
    int itemCount = 0;

    // Index - position
    // Element - value
    movies[0] = "My Neighbor Totoro";
    movies[1] = "The Matrix";
    movies[2] = "Arrival";
    itemCount = 3;

    // Iterate through all elements of the array, displaying each item
//    int counter = 0; // INITIALIZATION
//    while ( counter < itemCount ) // CONDITION
//    {
//        cout << counter << ". " << movies[counter] << endl;
//        counter++; // UPDATE_ACTION
//    }


    // For loop
    // for ( INITIALIZATION; CONDITION; UPDATE_ACTION )

    for ( int counter = 0; counter < itemCount; counter++ )
    {
        cout << counter << ". " << movies[counter] << endl;
    }


    // std::array
    // Don't need const int for max size, can access via .size() now.
    array<int, 15> auditoriumSeatCapacity;
    cout << auditoriumSeatCapacity.size() << endl;

    // Initializing values
    for ( int i = 0; i < auditoriumSeatCapacity.size(); i++ )
    {
        auditoriumSeatCapacity[i] = 0;
    }

    cout << "Update seating for which auditorium? (0-14): ";
//    int userChoice;
//    cin >> userChoice;

//    cout << "How many seats? ";
//    cin >> auditoriumSeatCapacity[userChoice];

    // Displaying all the values
    for ( int i = 0; i < auditoriumSeatCapacity.size(); i++ )
    {
        cout << i << ". " << auditoriumSeatCapacity[i] << endl;
    }

    return 0;
}
