#include <iostream>
using namespace std;

#include "functions.h"
// Don't include .cpp files

int main()
{
    int num1, num2;

    cout << "Enter num1: ";
    cin >> num1;

    cout << "Enter num2: ";
    cin >> num2;

//     if there is a return type
//     store result in a variable
    int result = Sum( num1, num2 );
    cout << "Result: " << result << endl;

    DisplayMenu();

    float money = 10.00;
    DisplayMoney( money );
    DisplayMoney( 1.23 );

    float tax = GetTax();

    money += money * tax;
    DisplayMoney( money );



    return 0;
}
