#include <iostream>     // console input and output (cin, cout)
#include <string>       // string
using namespace std;

int main()
{
    // STEP 1. CREATE PRODUCTS AND DISPLAY INVENTORY
    // Set up the products (Variable declaration and initialization)
    const float     item1Price      = 0.50;
    const string    item1Name       = "Pens";
    int             item1Quantity   = 50;

    const float     item2Price      = 1.99;
    const string    item2Name       = "Folders";
    int             item2Quantity   = 100;

    cout << "STORE INVENTORY" << endl;

    cout    << "Item:  " << item1Name
            << ", Price: " << item1Price
            << ", Quantity: " << item1Quantity
            << endl;

    cout    << "Item:  " << item2Name
            << ", Price: " << item2Price
            << ", Quantity: " << item2Quantity
            << endl << endl;



    // STEP 2. ASK USER HOW MANY OF EACH ITEM THEY WANT TO BUY
    int amountOfItem1ToBuy;
    int amountOfItem2ToBuy;

    cout << "PURCHASING" << endl;
    cout << "How many " << item1Name << "(s) do you want to buy? ";
    cin >> amountOfItem1ToBuy;

    cout << "How many " << item2Name << "(s) do you want to buy? ";
    cin >> amountOfItem2ToBuy;

    cout << endl;


    // STEP 3. ADJUST QUANTITY OF PRODUCTS
    item1Quantity = item1Quantity - amountOfItem1ToBuy;
    item2Quantity -= amountOfItem2ToBuy; // Does the same thing as above

    cout << "STORE INVENTORY" << endl;

    cout    << "Item:  " << item1Name
            << ", Price: " << item1Price
            << ", Quantity: " << item1Quantity
            << endl;

    cout    << "Item:  " << item2Name
            << ", Price: " << item2Price
            << ", Quantity: " << item2Quantity
            << endl << endl;


    // STEP 4. CALCULATE PRICES AND DISPLAY THE RECEIPT
    const float TAX_RATE = 0.09;

    float price = (amountOfItem1ToBuy * item1Price)
                    + (amountOfItem2ToBuy * item2Price);

    float tax = price * TAX_RATE;

    float total = price + tax;

    cout << "RECEIPT" << endl;

    cout << "Buying " << amountOfItem1ToBuy
            << " of " << item1Name << "(s)"
            << " at $" << item1Price << " each" << endl;

    cout << "Buying " << amountOfItem2ToBuy
            << " of " << item2Name << "(s)"
            << " at $" << item2Price << " each" << endl << endl;

    cout << "Subtotal:  $" << price << endl;
    cout << "Tax:       $" << tax << endl;
    cout << "Total:     $" << total << endl;





    return 0;
}
