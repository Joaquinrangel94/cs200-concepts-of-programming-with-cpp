#include <iostream>  // input/output stream library
#include <string>
using namespace std; // allow use of C++ standard functionality

int main()
{
    // Display text to the screen
    cout << "Hello, world!"
        << endl << "Goodbye, world!"
        << endl << "Just one more thing..." << endl;

    // Declaring a variable and initializing
    int myNumber = 2;
    float myFloat = 9.99;
    bool myBool = false;
    char myChar = '$';
    string myString = "Hello, world!";

    // Variable declaration
    int amountOfCats;

    // Assignment statement
    amountOfCats = 4;
    myNumber = amountOfCats;
    myFloat = 1.23;
    myBool = true;
    myChar = '%';
    myString = "JCCC";

    // Display variable values
    cout << "Amount of cats: " << amountOfCats << endl;
    cout << "My Float is: " << myFloat << endl;
    cout << "My Bool is: " << myBool << endl;
    cout << "My Char is: " << myChar << endl;
    cout << "My String is: " << myString << endl;

    // How to get input from user?
    string name;
    cout << "What is your name? ";
//    cin.ignore();
    getline( cin, name );

    int age;
    cout << "What is your age? ";
    cin >> age;

    float bankAccount;
    cout << "How much money do you have? ";
    cin >> bankAccount;


    // Display results
    cout << name << ", " << age << ", $" << bankAccount << endl;


    return 0;   // exit program
}
